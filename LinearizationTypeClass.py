#!/usr/bin/env python
# coding: utf-8

# # Define the Linearization Type Class

# In[1]:


import numpy as np

from enum import Enum

class LinearizationType(Enum):
    """Enumeration class defining linearization type"""
    Explicit = 1
    Implicit = 2

